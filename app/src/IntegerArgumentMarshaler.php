<?php

require_once('ArgumentMarshalerInterface.php');

class IntegerArgumentMarshaler implements ArgumentMarshalerInterface
{
    private $integerValue = 0;

    public function set($value)
    {
        $this->integerValue = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->integerValue;
    }
}