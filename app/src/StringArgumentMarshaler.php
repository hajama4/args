<?php

require_once('ArgumentMarshalerInterface.php');

class StringArgumentMarshaler implements ArgumentMarshalerInterface
{
    private $stringValue = "";

    public function set($value)
    {
        $this->stringValue = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->stringValue;
    }
}