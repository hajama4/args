<?php

class Args
{
    /**
     * @var array $marshalers
     */
    private $marshalers;

    /**
     * @param string $schema
     * @param array $args
     */
    public function mainArgs($schema, array $args)
    {
        $this->marshalers = [];
        $this->argsFound = [];

        $this->parseSchema($schema);
        $this->parseArgumentStrings($args);
    }

    /**
     * @param string $schema
     */
    private function parseSchema($schema)
    {
        $schema = explode(",", $schema);

        foreach ($schema as $s) {
            if (strlen($s) > 0) {
                $this->parseSchemaElement(trim($s));
            }
        }
    }

    /**
     * @param string $element
     */
    private function parseSchemaElement($element)
    {
        $elementId = $element[0];
        $elementTail = substr($element, 1);
        $this->validateSchemaElementId($elementId);

        if (strlen($elementTail) == 0) {
            $this->marshalers[$elementId] = new BooleanArgumentMarshaler();

            return;
        }

        switch ($elementTail) {
            case "*":
                $this->marshalers[$elementId] = new StringArgumentMarshaler();
                break;
            case "#":
                $this->marshalers[$elementId] = new IntegerArgumentMarshaler();
                break;
            default:
        }
    }

    /**
     * @param mixed $elementId
     */
    private function validateSchemaElementId($elementId)
    {
        if (is_numeric($elementId)) {
            //TODO NO NUMBERS Exception
        }
    }

    /**
     * @param array $argsList
     */
    private function parseArgumentStrings(array $argsList)
    {
        foreach ($argsList as $i => $args) {
            $value = $argsList[$i+1];
            if ($args[0] === "-") {
                $this->parseArgumentCharacter(substr($args, 1), $value);
            }
        }
    }

    /**
     * @param string $argChar
     * @param string $value
     */
    private function parseArgumentCharacter($argChar, $value)
    {
        $m = $this->marshalers[$argChar];

        if (is_null($m)) {

            return;
        }

        if ($m instanceof BooleanArgumentMarshaler) {
            $m->set(true);

            return;
        }

        $m->set($value);
    }

    /**
     * @param string $arg
     * @return bool
     */
    public function getBoolean($arg)
    {
        return $this->marshalers[$arg]->getValue();
    }

    /**
     * @param string $arg
     * @return string
     */
    public function getString($arg)
    {
        return $this->marshalers[$arg]->getValue();
    }

    /**
     * @param string $arg
     * @return integer
     */
    public function getInteger($arg)
    {
        return $this->marshalers[$arg]->getValue();
    }
}