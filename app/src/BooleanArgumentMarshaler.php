<?php

require_once('ArgumentMarshalerInterface.php');

class BooleanArgumentMarshaler implements ArgumentMarshalerInterface
{
    private $booleanValue = false;

    public function set($value = true)
    {
        $this->booleanValue = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->booleanValue;
    }
}