<?php

require_once('Args.php');
require_once('BooleanArgumentMarshaler.php');
require_once('StringArgumentMarshaler.php');
require_once('IntegerArgumentMarshaler.php');

$mainInstance = new Args();
$mainInstance->mainArgs("l,p#,s*", $argv);
$logging = $mainInstance->getBoolean("l") ? "True" : "False";
$directory = $mainInstance->getString("s");
$port = $mainInstance->getInteger("p");
printf("Logging: %s, Directory: %s, Port: %d", $logging, $directory, $port);